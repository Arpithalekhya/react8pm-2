import React from "react";
export class Player extends React.Component {
  state = {
    isColor: false
  };
  handleClick = () => {
    this.setState({
      isColor: !this.state.isColor
    });
  };
  render() {
    return (
      <li
        style={{color: this.state.isColor ? "red" : "blue"}}
        onClick={this.handleClick}
      >
        {this.props.player}
      </li>
    );
  }
}
