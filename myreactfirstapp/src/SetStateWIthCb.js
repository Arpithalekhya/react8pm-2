import React, {Component} from "react";

export default class SetStateWIthCb extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      name: "",
      data: []
    };
  }
  handleInc1 = () => {
    debugger;
    // this.setState((prev) => {
    //   return {
    //     count: prev.count + 1
    //   };
    // });
    this.setState((prev) => ({
      count: prev.count + 1
    }));
  };
  handleInc5 = () => {
    // this.setState({
    //   count: this.state.count + 5
    // });
    this.handleInc1();
    this.handleInc1();
    this.handleInc1();
    this.handleInc1();
    this.handleInc1();
  };

  render() {
    return (
      <React.Fragment>
        <button onClick={this.handleInc1}>Inc Count by 1</button>
        <h1>{this.state.count}</h1>
        <button onClick={this.handleInc5}>Inc Count by 5</button>
      </React.Fragment>
    );
  }
}
