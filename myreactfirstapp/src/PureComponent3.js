import React, {Component, PureComponent} from "react";

export default class PureComponent3 extends Component {
  state = {
    count: 0
  };
  handleClick = () => {
    this.setState((prev) => {
      return {
        count: prev.count + 1
      };
    });
  };
  render() {
    alert("Parent Got called");
    return (
      <div>
        <button onClick={this.handleClick}>CLick me</button>
        <h1>{this.state.count}</h1>
        <Child />
      </div>
    );
  }
}

class Child extends PureComponent {
  //   state = {};
  //   shouldComponentUpdate(nextProps, nextState) {
  //     if (nextState !== this.state && nextProps !== this.props) {
  //       return true;
  //     }
  //     return false;
  //   }
  render() {
    alert("CHILD GOT CALLED");
    return <div>I'm Child COmponent</div>;
  }
}
