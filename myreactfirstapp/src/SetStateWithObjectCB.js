import R from "react";
export class SetStateArgs extends R.Component {
  state = {
    count: 0
  };
  handleInc = () => {
    this.setState(
      {
        count: this.state.count + 1
      },
      () => console.log(this.state.count, "countInsideCB")
    );
    console.log(this.state.count, "countoutside of SetState");
  };
  render() {
    return (
      <div>
        <button onClick={this.handleInc}>Inc Count</button>
        <h1>{this.state.count}</h1>
      </div>
    );
  }
}
