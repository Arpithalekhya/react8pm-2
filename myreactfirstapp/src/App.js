// import React, {Component} from "react";
// import Footer from "./Components/Footer";
// import Header from "./Components/Header";
// import UseeffecthookExa from "./UseeffecthookExa";

import Footer from "./Components/Footer";
import Header from "./Components/Header/Header";
import Menu from "./Components/Menu/Menu";

// export default class App extends Component {
//   constructor(p) {
//     super();
//     this.state = {
//       name: p.name,
//       dataFromBackend: [],
//       count: 0
//     };
//   }
//   fn = () => {
//     this.setState({
//       name: "DHONI"
//     });
//   };
//   handleIncCount = () => {
//     this.setState({
//       count: this.state.count + 1
//     });
//   };
//   static getDerivedStateFromProps(nextProps, nextState) {
//     debugger;
//     if (nextState.count == 2) {
//       alert("called");
//       return {
//         name: "Ambani"
//       };
//     }
//   }

//   shouldComponentUpdate(Nextprops, nextState) {
//     debugger;
//     return true;
//   }

//   render() {
//     return (
//       <div>
//         App Component
//         {/* {this.state.dataFromBackend?.map((obj) => {
//           return <p>{obj.id}</p>;
//         })} */}
//         {/* <button onClick={this.fn}>Click me App</button>
//         <h1>{this.state.name}</h1>
//         <button onClick={this.handleIncCount}>Inc Count</button>
//         <h1>{this.state.count}</h1>
//         <UseeffecthookExa /> */}
//         <Header />
//         <Footer />
//       </div>
//     );
//   }
//   componentDidMount() {
//     console.log("componentDidMount");
//     fetch("https://jsonplaceholder.typicode.com/posts")
//       .then((res) => {
//         return res.json();
//       })
//       .then((data) => {
//         this.setState({
//           dataFromBackend: data
//         });
//       });
//   }
// }

const App = () => {
  debugger;
  return (
    <>
      <Header />
      <Menu />
      <Footer />
    </>
  );
};

export default App;
