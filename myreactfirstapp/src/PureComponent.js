import React, {Component, PureComponent} from "react";

export default class PureComponentExample extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: "Sachin",
      loc: "Mumbai"
    };
  }
  handleClick() {
    this.setState({
      name: "Sachin"
    });
  }
  //   shouldComponentUpdate(nextProps, nextState) {
  //     if (nextState.name !== this.state.name) {
  //       return true;
  //     }
  //     return false;
  //   }
  render() {
    alert("render got called");
    return (
      <p>
        <button onClick={this.handleClick.bind(this)}>Clickme</button>
        <h1>{this.state.name}</h1>
      </p>
    );
  }
}
