import React, {useState, memo} from "react";

function ReactMemo1() {
  const [name, setName] = useState("DHONI");
  const handleClick1 = () => {
    setName("SACHIN");
  };
  const handleClick2 = () => {
    setName("SACHIN");
  };
  return (
    <div>
      {alert("called")}
      <h1>{name}</h1>
      <button onClick={handleClick1}>click me1</button>
      <button onClick={handleClick2}>click me2</button>
    </div>
  );
}

export default ReactMemo1;
