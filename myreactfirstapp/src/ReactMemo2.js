import React, {useState, memo, useCallback} from "react";

function ReactMemo2() {
  const [count, setCount] = useState(0);
  const [data, setData] = useState([]);
  const handleInc = async () => {
    debugger;
    setCount(count + 1);
    // let a = await fetch("https://jsonplaceholder.typicode.com/posts").then(
    //   (res) => res.json()
    // );
    // setData(a);
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => res.json())
      .then((data) => {
        setData(data);
      });

    // let a = await fetch("https://jsonplaceholder.typicode.com/posts");
    // console.log(a.json(), "async");
    // let b = await a;
    // setData(b);
  };
  const fn = useCallback(() => {}, []);
  alert("Parent rendered");
  return (
    <div>
      <Child fn={fn} />
      <h1>{count}</h1>
      <button onClick={handleInc}>Click me</button>
      {data?.map((obj) => {
        return <h1>{obj.title}</h1>;
      })}
    </div>
  );
}

export default memo(ReactMemo2);

const Child = memo(({fn}) => {
  alert("Child rendered");
  return <div>I'm child Component</div>;
});
const name = "Sachin";
export const loc = "MUMBAI";

export {Child, name};
