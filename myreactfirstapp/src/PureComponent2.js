import React, {Component, PureComponent} from "react";
export default class PureComponent2 extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: "Sachin"
    };
    setTimeout(
      function () {
        this.setState({
          name: "DHONI"
        });
      }.bind(this),
      5000
    );

    setTimeout(() => {
      this.setState({
        name: "DHONI"
      });
    }, 10000);
  }
  render() {
    alert("render called");
    return <div style={{color: "red"}}>{this.state.name}</div>;
  }
}
