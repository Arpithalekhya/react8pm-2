import React, {useEffect, useState} from "react";

export default function UseeffecthookExa() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");

  useEffect(() => {}, [count]);

  useEffect(() => {}, [name]);

  const handleClick = () => {
    setCount(count + 1);
  };
  const handleName = () => {
    setName("Sachi");
  };
  return (
    <div>
      UseEffect
      <button onClick={handleClick}>Click me</button>
      <button onClick={handleName}>Change Name</button>
    </div>
  );
}
