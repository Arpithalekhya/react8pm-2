import React from "react";
import {Player} from "./Player";
class Child extends React.Component {
  constructor() {
    super();
    this.state = {
      player: "",
      playerList: []
    };
    this.playerResult = [];
  }

  onPlayerList = () => {
    let player = this.refs.player.value;
    this.refs.player.value = "";
    !this.playerResult.includes(player) && this.playerResult.push(player);
    // this.setState({
    //   player,
    //   playerList: this.state.playerList.includes(player)
    //     ? [...this.state.playerList]
    //     : [...this.state.playerList, player]
    // });
    this.setState({
      player,
      playerList: this.playerResult
    });
  };

  render() {
    return (
      <div>
        <input ref="player" />
        <button onClick={this.onPlayerList}>PlayerList</button>
        <h1>{this.state.player}</h1>
        <ul>
          {this.state.playerList.map((player, i) => {
            return <Player player={player} key={i} />;
          })}
        </ul>
      </div>
    );
  }
}
export default Child;
export const name = "Sachin";
export const location = "Mumbai";

// export {name, location};
