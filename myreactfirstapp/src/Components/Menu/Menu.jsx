import "./Menu.css";
import React, { lazy, Suspense } from "react";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom"
import ExeptionOccured1 from "../ExceptionHandling/ExeptionOccured1";
import WithoutJSX1 from "../WithoutJSX/WithoutJSX1";
import ContextAPI1 from "../ContextAPI/ContextAPI1";
// import Home from "../Home";
// import AboutUs from "../AboutUs";
const Home = lazy(() => { return import("../Home") })
const AboutUs = lazy(() => import("../AboutUs"))
const BtnHover = lazy(() => import("../HOC/BtnHover"))

function template() {
  const linkContent = [{ href: "/home", content: "Home" }, { href: "/about", content: "AboutUS" }, { href: "/eh", content: "EH" }, { href: "/hoc", content: "HOC" }, { href: "/jsx", content: "WithoutJSX" }, { href: "/contextAPI", content: "ContextAPI" }]
  const routeContent = [{ p: "/home", e: <Home /> }, { p: "/about", e: <AboutUs /> }, { p: "/eh", e: <ExeptionOccured1 /> }, { p: "hoc", e: <BtnHover /> }, { p: "/jsx", e: <WithoutJSX1 /> }, { p: "/contextAPI", e: <ContextAPI1 /> }]
  return (
    <div className="menu">
      <BrowserRouter>
        <Suspense fallback="loading......">
          {
            linkContent.map((obj) => {
              return <Link to={obj.href}>{obj.content}</Link>
            })
          }
          <Routes>
            {routeContent.map((obj) => {
              return <Route path={obj.p} element={obj.e} />
            })}
          </Routes>
        </Suspense>
      </BrowserRouter>

    </div>
  );
};


export default template;
