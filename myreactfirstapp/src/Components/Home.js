import React, {Component} from "react";

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
    this.listItems = [];
  }

  handleSave = () => {
    let name = this.refs.name.value;
    this.listItems = [...this.listItems, name];
    this.setState({
      data: [...this.state.data, name]
    });
    this.refs.name.value = "";
  };
  handleDelete = () => {
    let deleteItem = this.refs.deleteData.value;
    this.refs.deleteData.value = "";
    if (this.listItems.includes(deleteItem)) {
      let i = this.state.data.findIndex((name) => {
        return name === deleteItem;
      });
      this.listItems.splice(i, 1);
      this.setState({
        data: this.listItems
      });
    }
  };
  render() {
    return (
      <p>
        <input ref="name" />
        <button onClick={this.handleSave}>Save</button>
        {this.listItems.map((name, i) => {
          return <h1 key={i}>{name}</h1>;
        })}
        <input ref="deleteData" />
        <button onClick={this.handleDelete}>Delete</button>
      </p>
    );
  }
}
