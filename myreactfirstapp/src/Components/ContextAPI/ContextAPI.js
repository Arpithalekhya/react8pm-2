import React, {Component} from "react";

export default class ContextAPI extends Component {
  render() {
    const name = "Sachin";
    return (
      <div>
        <h1>ContextAPI</h1>
        <A data={name} />
      </div>
    );
  }
}

class A extends Component {
  render() {
    return (
      <>
        <h1>A</h1>
        <B name={this.props.data} />
      </>
    );
  }
}

class B extends Component {
  render() {
    return (
      <>
        <h1>B</h1>
        <C d={this.props.name} />
      </>
    );
  }
}

class C extends Component {
  render() {
    return (
      <>
        <h1>C</h1>
        <D name={this.props.d} />
      </>
    );
  }
}
class D extends Component {
  render() {
    return (
      <>
        <h1>I'm D Component.....{this.props.name}</h1>
      </>
    );
  }
}
