import React, {Component, createContext} from "react";

const myCtx = createContext();

export default class ContextAPI extends Component {
  render() {
    const name = "Sachin";
    return (
      <div>
        <h1>ContextAPI</h1>
        <myCtx.Provider value={name}>
          <A />
        </myCtx.Provider>
      </div>
    );
  }
}

class A extends Component {
  render() {
    return (
      <>
        <h1>A</h1>
        <B />
      </>
    );
  }
}

class B extends Component {
  render() {
    return (
      <>
        <h1>B......{this.context}</h1>
        <C />
      </>
    );
  }
}
B.contextType = myCtx;

class C extends Component {
  render() {
    return (
      <>
        <myCtx.Consumer>
          {(data) => {
            return <h1>I'm C Component.....{data}</h1>;
          }}
        </myCtx.Consumer>
        <D />
      </>
    );
  }
}
class D extends Component {
  render() {
    return (
      <>
        <myCtx.Consumer>
          {(data) => {
            return <h1>I'm D Component.....{data}</h1>;
          }}
        </myCtx.Consumer>
      </>
    );
  }
}
