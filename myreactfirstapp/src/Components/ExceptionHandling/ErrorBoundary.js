import React, {Component} from "react";

export default class ErrorBoundary extends Component {
  state = {
    isError: false
  };
  static getDerivedStateFromError() {
    return {
      isError: true
    };
  }

  componentDidCatch(e) {
    console.log(e);
  }
  render() {
    return (
      <div>
        {this.state.isError ? "Something went wrong" : this.props.children}
      </div>
    );
  }
}
