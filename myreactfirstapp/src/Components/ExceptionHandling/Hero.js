import React, {Component} from "react";

export default class Hero extends Component {
  render() {
    if (this.props.hero === "jiranjeevi") {
      throw new Error("Jiranjeevi is not acceptable");
    }
    return <div>{this.props.hero}</div>;
  }
}
