import React, {Component} from "react";
import ErrorBoundary from "./ErrorBoundary";
import Hero from "./Hero";

export default class ExeptionOccured1 extends Component {
  render() {
    return (
      <div>
        <ErrorBoundary>
          <Hero hero="Prabhas" />
        </ErrorBoundary>
        <ErrorBoundary>
          <Hero hero="Allu arj" />
        </ErrorBoundary>
        <ErrorBoundary>
          <Hero hero="NTR" />
        </ErrorBoundary>
        <ErrorBoundary>
          <Hero hero="jiranjeevi" />
        </ErrorBoundary>
      </div>
    );
  }
}
