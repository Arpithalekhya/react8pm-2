import React, {Component} from "react";
import Hero from "./Hero";

export default class ExceptionOccured extends Component {
  constructor() {
    super();
    this.state = {
      isError: false
    };
  }
  static getDerivedStateFromError() {
    return {
      isError: true
    };
  }
  componentDidCatch(e) {
    alert(e);
  }
  render() {
    return (
      <div>
        {this.state.isError ? (
          "Something went wrong"
        ) : (
          <>
            {" "}
            <Hero hero="Prabhas" />
            <Hero hero="Allu arj" />
            <Hero hero="NTR" />
            <Hero hero="jiranjeevi" />
          </>
        )}
      </div>
    );
  }
}
