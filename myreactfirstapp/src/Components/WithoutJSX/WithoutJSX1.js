import React, {Component, createElement} from "react";

export default class WithoutJSX1 extends Component {
  render() {
    return (
      <>
        {createElement("div", {id: "myDiv", className: "myClass"}, "Mumbai")}
        {createElement(Hellow, {name: "Sachin"}, null)}
      </>
    );
  }
}

class Hellow extends Component {
  render() {
    return (
      <>
        {createElement("div", null, [
          createElement("span", null, "Bangalore"),
          createElement("span", null, "Hyderabad")
        ])}
      </>
    );
  }
}
