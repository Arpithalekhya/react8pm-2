import React, {Component} from "react";
import myHoc from "./Hoc";

class HoverComp extends Component {
  render() {
    return (
      <div>
        <h1 onMouseOver={this.props.fn}>
          Hover to Inc count{this.props.count}
        </h1>
      </div>
    );
  }
}

export default myHoc(HoverComp);
