import React, {Component} from "react";
import myHoc from "./Hoc";

class ButtonComp extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.fn}>
          I'm button Component {this.props.count}
        </button>
      </div>
    );
  }
}

export default myHoc(ButtonComp);
