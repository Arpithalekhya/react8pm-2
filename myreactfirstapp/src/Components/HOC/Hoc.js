import React from "react";

const myHoc = (Comp) => {
  class Logic extends React.Component {
    constructor() {
      super();
      this.state = {
        count: 0
      };
    }

    incCount = () => {
      this.setState({
        count: this.state.count + 1
      });
    };
    render() {
      return (
        <div>
          <Comp count={this.state.count} fn={this.incCount} />
        </div>
      );
    }
  }
  return Logic;
};
export default myHoc;
