import React, {Component} from "react";

export default class Players extends Component {
  render() {
    return (
      <>
        {this.props.name}
        <h2>{this.props.children}</h2>
      </>
    );
  }
}

export const Plyers = ({name, children}) => {
  return (
    <>
      <h1>{name}</h1>
      <h2>{children}</h2>
    </>
  );
};
