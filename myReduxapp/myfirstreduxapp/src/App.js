import {useState} from "react";
import "./App.css";
import A from "./Components/A";
import B from "./Components/B";
import C from "./Components/C";

function App() {
  const [name, setName] = useState("");
  const [loc, setLoc] = useState("");

  const getName = (data) => {
    setName(data);
  };

  const getLocation = (data) => {
    setLoc(data);
  };
  return (
    <div className="App">
      <h1>App</h1>
      <A gN={getName} />
      <B gL={getLocation} />
      <C n={name} l={loc} />
    </div>
  );
}

export default App;
