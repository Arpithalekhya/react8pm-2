import {init} from "../Utils/init";

const reducer = (state = init, action) => {
  if (action.type === "NAME") {
    state = {
      ...state,
      name: action.payload
    };
  }
  if (action.type === "LOC") {
    state = {
      ...state,
      location: action.data
    };
  }
  console.log(state, "redux state");
  return state;
};

export default reducer;
