import {combineReducers} from "redux";
import myReducer from "./myReducer";
import colorReducer from "./colorReducer";

const rootReducer = combineReducers({
  mR: myReducer,
  colorReducer
});

export default rootReducer;
