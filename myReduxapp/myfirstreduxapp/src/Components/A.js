import React, {Component} from "react";
import {store} from "../Store/myStore";

export default class A extends Component {
  handleName = () => {
    let name = this.refs.name.value;
    // this.props.gN(name);
    store.dispatch({
      type: "NAME",
      payload: name
    });
  };
  render() {
    return (
      <p>
        <input ref="name" />
        <button onClick={this.handleName}>SetName</button>
      </p>
    );
  }
}
