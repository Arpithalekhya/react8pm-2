import React, {Component} from "react";
import {connect} from "react-redux";

class C extends Component {
  render() {
    return (
      <div>
        {/* <h1>GET Name:{this.props.n}</h1>
        <h1>GET location:{this.props.l}</h1> */}
        <h1>GET Name:{this.props.name}</h1>
        <h1>GET location:{this.props.loc}</h1>
      </div>
    );
  }
}

const msp = (state) => {
  return {
    name: state.mR.name,
    loc: state.mR.location
  };
};
const mdp = () => {};
export default connect(msp, mdp)(C);
