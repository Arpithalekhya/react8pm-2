import React, {Component, createRef} from "react";
import {store} from "../Store/myStore";

export default class B extends Component {
  constructor() {
    super();
    this.myLocationRef = createRef();
  }
  handleLocation = () => {
    let loc = this.myLocationRef.current.value;
    // this.props.gL(loc);
    store.dispatch({
      type: "LOC",
      data: loc
    });
  };
  render() {
    return (
      <div>
        <input ref={this.myLocationRef} />
        <button onClick={this.handleLocation}>SetLocation</button>
      </div>
    );
  }
}
